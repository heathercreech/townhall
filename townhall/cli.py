"""Console script for townhall."""
import sys
import click
import discord
from discord.ext import tasks
from dotenv import load_dotenv, dotenv_values
from hashlib import sha3_224
from time import time
from uuid import uuid4

SERVER_ID = "796954275158163506"
CHANNEL_ID = "796959208528019466"

# Set to one day
SECONDS_TO_EXPIRE_SESSION = 86400

# Set to one hour
SECONDS_TO_RUN_SESSION_CLEANUP_CHECK = 3600


class Session:
    def __init__(self, username):
        start_time = time()
        random_salt = uuid4()
        hash_generator = sha3_224()
        hash_generator.update(username)
        hash_generator.update(str(start_time).encode("ascii"))
        # Adding a random salt in here so that no one can take a user's name and guess a timestamp
        # to try and generate the hash we'll display
        hash_generator.update(str(random_salt).encode("ascii"))

        self.user_session = {
            "hashed_username": hash_generator.hexdigest(),
            "session_start": start_time,
            "expires": start_time + SECONDS_TO_EXPIRE_SESSION,
        }


class TownHallClient(discord.Client):
    sessions = {}
    primary_server: discord.Guild = None
    log_channel: discord.TextChannel = None
    allowed_role: str = None
    config = None
    is_debug_mode_on = False
    is_startup_loop = True

    def __init__(self, config):
        super().__init__()
        self.config = config
        self.is_debug_mode_on = config["DEBUG"] == "true"

    async def on_ready(self):
        click.echo("Logged on as {0}".format(self.user))
        self.primary_server = await self.fetch_guild(self.config["SERVER_ID"])
        self.log_channel = await self.fetch_channel(self.config["LOG_CHANNEL_ID"])
        self.allowed_role = self.primary_server.get_role(
            int(self.config["ALLOWED_ROLE_ID"])
        )
        await self.change_presence(
            status=discord.Status.online,
            activity=discord.CustomActivity(name=self.config["PRESENCE_STRING"]),
        )
        self.clean_up_expired_sessions.start()

    async def on_message(self, message: discord.Message):
        """
        Will check for DMs, that the discord user has the correct privileges,
        and then send their message to the designated log channel if they pass.
        """
        if message.channel.type == discord.ChannelType.private:
            if message.author.name not in self.sessions:
                self.sessions[message.author.name] = Session(
                    message.author.name.encode("ascii")
                )

            member: discord.Member = await self.primary_server.fetch_member(
                message.author.id
            )
            if member is not None and self.allowed_role in member.roles:
                anon_message_content = f"**[{self.sessions[message.author.name].user_session['hashed_username'][0:16]}...]**: {message.content}"
                await self.log_channel.send(anon_message_content)
                click.echo(anon_message_content)
            else:
                # We're only logging names in case of an unauthorized usage. This is for security
                # and will not out legitimate users. Will help with debugging role access privileges as well
                unauthorized_message_content = f"Attempt to send message by unauthorized discord user: {member.name}#{member.discriminator}"
                click.echo(unauthorized_message_content)
                if self.is_debug_mode_on:
                    await self.log_channel.send(
                        f"**[DEBUG]**: {unauthorized_message_content}"
                    )

    @tasks.loop(seconds=SECONDS_TO_RUN_SESSION_CLEANUP_CHECK)
    async def clean_up_expired_sessions(self):
        if self.is_startup_loop:
            self.is_startup_loop = False
            return

        click.echo("Cleaning up expired sessions")

        if self.is_debug_mode_on:
            await self.log_channel.send("**[DEBUG]**: Cleaning up expired sessions")
        self.sessions = {}


@click.command()
def main(args=None):
    """Console script for townhall."""
    load_dotenv()
    config = dotenv_values(".env")
    client = TownHallClient(config)
    client.run(config["BOT_TOKEN"])
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
